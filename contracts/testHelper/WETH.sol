pragma solidity 0.8.17;

interface WETH {
   function balanceOf(address _user) external view returns(uint);
}