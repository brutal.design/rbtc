const Token = artifacts.require("RabbitCoin");
const Presale = artifacts.require("Presale");
const AirdropHelper = artifacts.require("AirdropHelper");
const StakeBnbRabbitCoin = artifacts.require("StakeBnbRabbitCoin");
const StakeRabbitCoin = artifacts.require("StakeRabbitCoin");
const AddressFrom = artifacts.require("AddressFrom");

module.exports = async function (deployer, network, accounts) {
  const DEFAULT = accounts[0];

  // token
  const name = "Rabbit Coin";
  const symbol = "RABTc";
  const transferTax = 1; // [0 - 100]
  const sellPercent = 2; // [0 - 100]
  let presaleContract;
  let stakeBnbRabbitCoinContract;
  let stakeRabbitCoinContract;
  const developerWallet = "0x0000000000000000000000000000000000000004";
  const marketingWallet = "0x0000000000000000000000000000000000000044";
  const donationWallet = "0x0000000000000000000000000000000000000055";
  const totalSupply = "100";

  // presale
  const coinsPerBNB = 99; // price how much tokens user recive for 1 bnb

  // stake LP BnbRabbitCoin 
  const rewardPerBlockForBnbRabbitCoin = 33;

  // stake RabbitCoin 
  const rewardPerBlockForRabbitCoin = 44;

  // deploy part
  await deployer.deploy(AddressFrom);
  const addressFromInstance = await AddressFrom.deployed();
  presaleContract = await addressFromInstance.addressFrom(DEFAULT, await web3.eth.getTransactionCount(DEFAULT) + 1);
  stakeBnbRabbitCoinContract = await addressFromInstance.addressFrom(DEFAULT, await web3.eth.getTransactionCount(DEFAULT) + 2);
  stakeRabbitCoinContract = await addressFromInstance.addressFrom(DEFAULT, await web3.eth.getTransactionCount(DEFAULT) + 3);


  await deployer.deploy(Token, name, symbol, transferTax, sellPercent, presaleContract, stakeBnbRabbitCoinContract, stakeRabbitCoinContract, developerWallet, marketingWallet, donationWallet, totalSupply);
  const tokenInstance = await Token.deployed()

  await deployer.deploy(Presale, tokenInstance.address, developerWallet, coinsPerBNB);
  const presaleInstance = await Presale.deployed()

  await deployer.deploy(StakeBnbRabbitCoin, await tokenInstance.pair(), tokenInstance.address, rewardPerBlockForBnbRabbitCoin);
  const stakeBnbRabbitCoinInstance = await StakeBnbRabbitCoin.deployed();

  await deployer.deploy(StakeRabbitCoin, tokenInstance.address, tokenInstance.address, rewardPerBlockForRabbitCoin);
  const stakeRabbitCoinInstance = await StakeRabbitCoin.deployed();

  await deployer.deploy(AirdropHelper);
  const airdropHelperInstance = await AirdropHelper.deployed();

  // just for sure
  console.log("expected and real presale:", "\n"+presaleContract, "\n"+presaleInstance.address, "\n");
  console.log("expected and real stakeBnbRabbitCoinContract:", "\n"+stakeBnbRabbitCoinContract, "\n"+stakeBnbRabbitCoinInstance.address, "\n");
  console.log("expected and real stakeRabbitCoinInstance:", "\n"+stakeRabbitCoinContract, "\n"+stakeRabbitCoinInstance.address, "\n");

  console.log("\ntoken check:");
  console.log("expected and real name:", "\n"+name, "\n"+ await tokenInstance.name());
  console.log("expected and real symbol:", "\n"+symbol, "\n"+ await tokenInstance.symbol());
  console.log("expected and real transferTax:", "\n"+transferTax, "\n"+ await tokenInstance.transferTax());
  console.log("expected and real sellPercent:", "\n"+sellPercent, "\n"+ await tokenInstance.sellPercent());
  console.log("expected and real presaleContract:", "\n"+presaleContract, "\n"+ await tokenInstance.presaleContract());
  console.log("expected and real stakeBnbRabbitCoinContract:", "\n"+stakeBnbRabbitCoinContract, "\n"+ await tokenInstance.stakeBnbRabbitCoinContract());
  console.log("expected and real stakeRabbitCoinContract:", "\n"+stakeRabbitCoinContract, "\n"+ await tokenInstance.stakeRabbitCoinContract());
  console.log("expected and real developerWallet:", "\n"+developerWallet, "\n"+ await tokenInstance.developerWallet());
  console.log("expected and real marketingWallet:", "\n"+marketingWallet, "\n"+ await tokenInstance.marketingWallet());
  console.log("expected and real donationWallet:", "\n"+donationWallet, "\n"+ await tokenInstance.donationWallet());
  console.log("expected and real totalSupply:", "\n"+totalSupply, "\n"+ await tokenInstance.totalSupply());

  console.log("\npresale check:");
  console.log("expected and real RABTc:", "\n"+tokenInstance.address, "\n"+ await presaleInstance.RABTc());
  console.log("expected and real developerWallet:", "\n"+developerWallet, "\n"+ await presaleInstance.developerWallet());
  console.log("expected and real coinsPerBNB:", "\n"+coinsPerBNB, "\n"+ await presaleInstance.coinsPerBNB());

  console.log("\nstakeBnbRabbitCoin check:");
  console.log("expected and real stakeToken:", "\n"+await tokenInstance.pair(), "\n"+ await stakeBnbRabbitCoinInstance.stakeToken());
  console.log("expected and real distributionToken:", "\n"+tokenInstance.address, "\n"+ await stakeBnbRabbitCoinInstance.distributionToken());
  console.log("expected and real rewardPerBlockForBnbRabbitCoin:", "\n"+rewardPerBlockForBnbRabbitCoin, "\n"+ await stakeBnbRabbitCoinInstance.rewardPerBlock());

  console.log("\nstakeRabbitCoin check:");
  console.log("expected and real stakeToken:", "\n"+tokenInstance.address, "\n"+ await stakeRabbitCoinInstance.stakeToken());
  console.log("expected and real distributionToken:", "\n"+tokenInstance.address, "\n"+ await stakeRabbitCoinInstance.distributionToken());
  console.log("expected and real rewardPerBlockForRabbitCoin:", "\n"+rewardPerBlockForRabbitCoin, "\n"+ await stakeRabbitCoinInstance.rewardPerBlock());

  console.log("\n\n\n\n\n");
  console.log("contracts:");
  console.log("Token", tokenInstance.address);
  console.log("Presale", presaleInstance.address);
  console.log("stakeBnbRabbitCoin", stakeBnbRabbitCoinInstance.address);
  console.log("stakeRabbitCoin", stakeRabbitCoinInstance.address);
  console.log("AirdropHelper", airdropHelperInstance.address);

}