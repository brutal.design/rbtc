const Presale = artifacts.require("Presale");
const Token = artifacts.require("RabbitCoin");
const AddressFrom = artifacts.require("AddressFrom");

const Reverter = require("./helpers/reverter");
const truffleAssertions = require("truffle-assertions");
const BigNumber = require("bignumber.js");

const toBN = (num) => {
  return new BigNumber(num);
};

contract("Presale", async (accounts) => {
  const reverter = new Reverter(web3);

  // presale deploy
  const developerWallet = "0xc23Ed325Bf5FE6144e466c571FFd7CB4e4eFC0eC";
  const coinsPerBNB = 120;
  const ONE_BNB = toBN("1000000000000000000");

  // rabbit coin deploy
  const name = "Rabbit Coin";
  const symbol = "RABTc";
  const transferTax = 20; // [0 - 100]
  const sellPercent = 0; // [0 - 100]
  let presaleContract;
  const stakeBnbRabbitCoinContract = "0x0000000000000000000000000000000000000002";
  const stakeRabbitCoinContract = "0x0000000000000000000000000000000000000003";
  const marketingWallet = "0x01824A4cB1760AA579Da73434aE231946c29e74B";
  const donationWallet = "0xF0b8150123F1c58c5373f48b63E7a8eBa89b8086";
  const totalSupply = toBN("1000000000000000000000000000000");

  const DEFAULT = accounts[0];

  let presale;
  let token;
  let addressFrom;

  before("setup", async () => {
    addressFrom = await AddressFrom.new();
    presaleContract = await addressFrom.addressFrom(DEFAULT, await web3.eth.getTransactionCount(DEFAULT) + 1);
    

    token = await Token.new(name, symbol, transferTax, sellPercent, presaleContract, stakeBnbRabbitCoinContract, stakeRabbitCoinContract, developerWallet, marketingWallet, donationWallet, totalSupply);
    presale = await Presale.new(token.address, developerWallet, coinsPerBNB);
    await reverter.snapshot();
  });

  afterEach("revert", reverter.revert);

  describe("constructor", async () => {
    it("shoult correctly set values", async () => {
      assert.equal(token.address, await presale.RABTc());
      assert.equal(developerWallet, await presale.developerWallet());
      assert.equal(coinsPerBNB, await presale.coinsPerBNB());
    });
  });

  describe("receive", async () => {
    it("should buy tokens", async () => {
      const JOHN = accounts[1];
      await presale.sendTransaction({from: JOHN, value: ONE_BNB});

      assert.equal(ONE_BNB, await web3.eth.getBalance(developerWallet));
      assert.equal(0, await web3.eth.getBalance(presale.address));
      assert.equal(ONE_BNB.multipliedBy(coinsPerBNB).toString(), (await token.balanceOf(JOHN)).toString());
    });
    it("should buy tokens and recive bonus 0.4 bnb (value is 4)", async () => {
      const JOHN = accounts[1];
      const amountToSend = ONE_BNB.multipliedBy(4);
      const amountAfterBonus = ONE_BNB.multipliedBy(36).dividedBy(10);
      const bonusAmount = ONE_BNB.dividedBy(10).multipliedBy(4);

      const johnBalanceBefore = await web3.eth.getBalance(JOHN);
      const tx = await presale.sendTransaction({from: JOHN, value: amountToSend});

      assert.equal((bonusAmount).plus(johnBalanceBefore), toBN(tx.receipt.gasUsed).plus(await web3.eth.getBalance(JOHN)).plus(amountToSend).toString());
      assert.equal(amountAfterBonus, await web3.eth.getBalance(developerWallet));
      assert.equal(0, await web3.eth.getBalance(presale.address));
      assert.equal(amountToSend.multipliedBy(coinsPerBNB).toString(), (await token.balanceOf(JOHN)).toString());
    });
    it("should buy tokens and recive bonus 0.4 bnb (value is 7.9)", async () => {
      const JOHN = accounts[1];
      const amountToSend = ONE_BNB.multipliedBy(7.9);
      const amountAfterBonus = ONE_BNB.multipliedBy(7.5);
      const bonusAmount = ONE_BNB.dividedBy(10).multipliedBy(4);

      const johnBalanceBefore = await web3.eth.getBalance(JOHN);
      const tx = await presale.sendTransaction({from: JOHN, value: amountToSend});

      assert.equal((bonusAmount).plus(johnBalanceBefore), toBN(tx.receipt.gasUsed).plus(await web3.eth.getBalance(JOHN)).plus(amountToSend).toString());
      assert.equal(amountAfterBonus, await web3.eth.getBalance(developerWallet));
      assert.equal(0, await web3.eth.getBalance(presale.address));
      assert.equal(amountToSend.multipliedBy(coinsPerBNB).toString(), (await token.balanceOf(JOHN)).toString());
    });
    it("should buy tokens and recive bonus 1 bnb (value is 8)", async () => {
      const JOHN = accounts[1];
      const amountToSend = ONE_BNB.multipliedBy(8);
      const amountAfterBonus = ONE_BNB.multipliedBy(7);
      const bonusAmount = ONE_BNB;

      const johnBalanceBefore = await web3.eth.getBalance(JOHN);
      const tx = await presale.sendTransaction({from: JOHN, value: amountToSend});

      assert.equal((bonusAmount).plus(johnBalanceBefore), toBN(tx.receipt.gasUsed).plus(await web3.eth.getBalance(JOHN)).plus(amountToSend).toString());
      assert.equal(amountAfterBonus, await web3.eth.getBalance(developerWallet));
      assert.equal(0, await web3.eth.getBalance(presale.address));
      assert.equal(amountToSend.multipliedBy(coinsPerBNB).toString(), (await token.balanceOf(JOHN)).toString());
    });
    it("should buy tokens and recive bonus 1 bnb (value is 11.9)", async () => {
      const JOHN = accounts[1];
      const amountToSend = ONE_BNB.multipliedBy(11.9);
      const amountAfterBonus = ONE_BNB.multipliedBy(10.9);
      const bonusAmount = ONE_BNB;

      const johnBalanceBefore = await web3.eth.getBalance(JOHN);
      const tx = await presale.sendTransaction({from: JOHN, value: amountToSend});

      assert.equal((bonusAmount).plus(johnBalanceBefore), toBN(tx.receipt.gasUsed).plus(await web3.eth.getBalance(JOHN)).plus(amountToSend).toString());
      assert.equal(amountAfterBonus, await web3.eth.getBalance(developerWallet));
      assert.equal(0, await web3.eth.getBalance(presale.address));
      assert.equal(amountToSend.multipliedBy(coinsPerBNB).toString(), toBN(await token.balanceOf(JOHN)).toLocaleString());
    });
    it("should buy tokens and recive bonus 2 bnb (value is 12)", async () => {
      const JOHN = accounts[1];
      const amountToSend = ONE_BNB.multipliedBy(12);
      const amountAfterBonus = ONE_BNB.multipliedBy(10);
      const bonusAmount = ONE_BNB.multipliedBy(2);

      const johnBalanceBefore = await web3.eth.getBalance(JOHN);
      const tx = await presale.sendTransaction({from: JOHN, value: amountToSend});

      assert.equal((bonusAmount).plus(johnBalanceBefore), toBN(tx.receipt.gasUsed).plus(await web3.eth.getBalance(JOHN)).plus(amountToSend).toString());
      assert.equal(amountAfterBonus, await web3.eth.getBalance(developerWallet));
      assert.equal(0, await web3.eth.getBalance(presale.address));
      assert.equal(amountToSend.multipliedBy(coinsPerBNB).toString(), toBN(await token.balanceOf(JOHN)).toLocaleString());
    });
    it("should get exception, sent less than 1 bnb", async () => {
      const revertMsg = "sent less than 1 bnb";

      await truffleAssertions.reverts(presale.sendTransaction({value: ONE_BNB.minus(1)}), revertMsg)
    });
  });

  describe("destroyPresale", async () => {
    it("should burn all tokens and destroy himself", async () => {
      assert.isTrue(0 != await token.balanceOf(presale.address));
      await presale.destroyPresale();
      assert.isTrue(0 == await token.balanceOf(presale.address));
    });
  });
});
