const Token = artifacts.require("RabbitCoin");
const Pair = artifacts.require("IPair");
const WETH = artifacts.require("WETH");
const Router = artifacts.require("PancakeRouter");

const Reverter = require("./helpers/reverter");
const truffleAssertions = require("truffle-assertions");
const BigNumber = require("bignumber.js");

const toBN = (num) => {
  return new BigNumber(num);
};

contract("RabbitCoin", async (accounts) => {
  const reverter = new Reverter(web3);

  const hundredPercent = 100;
  const ONE_BNB = toBN("1000000000000000000");

  const name = "Rabbit Coin";
  const symbol = "RABTc";
  const transferTax = 20; // [0 - 100]
  const sellPercent = 15; // [0 - 100]
  const presaleContract = "0xC538dd8364B7D3ea01509Ee9A7f6EEf2b4C8F905";
  const stakeBnbRabbitCoinContract = "0x0000000000000000000000000000000000000002";
  const stakeRabbitCoinContract = "0x0000000000000000000000000000000000000003";
  const developerWallet = accounts[1];
  const marketingWallet = "0x01824A4cB1760AA579Da73434aE231946c29e74B";
  const donationWallet = "0xF0b8150123F1c58c5373f48b63E7a8eBa89b8086";
  const totalSupply = toBN("1000000000000000000000000000000");

  const DEFAULT = accounts[0];
  const ALICE = accounts[2];
  const BOB = accounts[3];
  const BURN = "0x0000000000000000000000000000000000000000";

  let token;
  let weth;
  let router;

  const timestamp = () =>{
    return new Date().getTime();
}

  before("setup", async () => {
    token = await Token.new(name, symbol, transferTax, sellPercent, presaleContract, stakeBnbRabbitCoinContract, stakeRabbitCoinContract, developerWallet, marketingWallet, donationWallet, totalSupply);
    weth = await WETH.at("0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c");
    router = await Router.at("0x10ED43C718714eb63d5aA57B78B54704E256024E");
    await reverter.snapshot();
  });

  afterEach("revert", reverter.revert);

  describe("constructor", async () => {
    it("shoult correctly set values", async () => {
      assert.equal(name, await token.name());
      assert.equal(symbol, await token.symbol());
      assert.equal(transferTax, await token.transferTax());
      assert.equal(sellPercent, await token.sellPercent());
      assert.equal(developerWallet, await token.developerWallet());
      assert.equal(marketingWallet, await token.marketingWallet());
      assert.equal(donationWallet, await token.donationWallet());
      assert.equal(totalSupply.toLocaleString(), toBN(await token.totalSupply()).toLocaleString());

      assert.equal(presaleContract, await token.presaleContract());
      assert.equal(stakeBnbRabbitCoinContract, await token.stakeBnbRabbitCoinContract());
      assert.equal(stakeRabbitCoinContract, await token.stakeRabbitCoinContract());

      assert.isTrue(await token.whitelist(developerWallet));
      assert.isTrue(await token.whitelist(marketingWallet));
      assert.isTrue(await token.whitelist(donationWallet));

      assert.equal(totalSupply.div(100).multipliedBy(90).toLocaleString(), toBN(await token.balanceOf(developerWallet)).toLocaleString());
      assert.equal(totalSupply.div(100).multipliedBy(10).toLocaleString(), toBN(await token.balanceOf(presaleContract)).toLocaleString());
    });
  });

  describe("setWhitelist", async () => {
    it("should set whitelist", async () => {
      const randomAddress = "0x0000000000000000000000000000000987654321";
      assert.isFalse(await token.whitelist(randomAddress));

      await token.setWhitelist(randomAddress, true);
      assert.isTrue(await token.whitelist(randomAddress));

      await token.setWhitelist(randomAddress, false);
      assert.isFalse(await token.whitelist(randomAddress));
    });
  });

  describe("changeTransferTax", async () => {
    it("should change transfer tax", async () => {
      const newFirstAmount = 777;
      const newSecondAmount = 999;

      await token.changeTransferTax(newFirstAmount);
      assert.equal(newFirstAmount, await token.transferTax());

      await token.changeTransferTax(newSecondAmount);
      assert.equal(newSecondAmount, await token.transferTax());
    });
  });

  describe("changeSellPercent", async () => {
    it("should change sell percent", async () => {
      const newFirstAmount = 777;
      const newSecondAmount = 999;

      await token.changeSellPercent(newFirstAmount);
      assert.equal(newFirstAmount, await token.sellPercent());

      await token.changeSellPercent(newSecondAmount);
      assert.equal(newSecondAmount, await token.sellPercent());
    });
  });

  describe("_afterTokenTransfer", async () => {
    it("shoul send tokens without fee (sender from whitelist) and with fee (sender not in whitelist)", async () => {
      const amount = 100;

      assert.isTrue(await token.whitelist(developerWallet));
      await token.transfer(ALICE, amount, {from: developerWallet});
      assert.equal(amount, await token.balanceOf(ALICE));

      assert.isFalse(await token.whitelist(ALICE));
      await token.transfer(BOB, amount, {from: ALICE});
      assert.equal(amount - (amount / hundredPercent * transferTax), await token.balanceOf(BOB));
    });
    it("not whitelist user should burn without any problems", async () => {
      const amount = 10;

      assert.isFalse(await token.whitelist(ALICE));
      await token.transfer(ALICE, amount, {from: developerWallet});
      await token.burn(amount, {from: ALICE});
    });
  });

  describe("addLiquidity/removeLiquidity", async () => {
    it("shoul add liquidity from non whitelist address and fee shouldn't be taken", async () => {
      const amount = ONE_BNB;

      await token.transfer(ALICE, amount, {from: developerWallet});
      assert.equal(amount, (await token.balanceOf(ALICE)).toString());
      assert.isFalse(await token.whitelist(ALICE));

      await token.addLiquidity(amount, {from: ALICE, value: amount});
      assert.equal(0, await token.balanceOf(ALICE));
      assert.equal(amount, (await token.balanceOf(await token.pair())).toString());
      assert.equal(amount, (await weth.balanceOf(await token.pair())).toString());
    });
    it("shoul add liquidity and send back part of bnb", async () => {
      const amount = ONE_BNB;

      await token.transfer(ALICE, amount, {from: developerWallet});
      await token.addLiquidity(amount, {from: ALICE, value: amount});
      assert.equal(amount, (await token.balanceOf(await token.pair())).toString());
      assert.equal(amount, (await weth.balanceOf(await token.pair())).toString());

      const bnbAmount = amount.multipliedBy(10);
      const bobBalanceBefore = await web3.eth.getBalance(BOB);
      await token.transfer(BOB, amount, {from: developerWallet});
      const tx = await token.addLiquidity(amount, {from: BOB, value: bnbAmount});
      assert.equal(amount.multipliedBy(2), (await token.balanceOf(await token.pair())).toString());
      assert.equal(amount.multipliedBy(2), (await weth.balanceOf(await token.pair())).toString());
      assert.equal(await web3.eth.getBalance(BOB), toBN(bobBalanceBefore).minus(tx.receipt.gasUsed).minus(amount));
    });
    it("shoul add liquidity from non whitelist address, then remove and fee shouldn't be taken", async () => {
      const amount = ONE_BNB;

      await token.transfer(ALICE, amount, {from: developerWallet});
      assert.equal(amount, (await token.balanceOf(ALICE)).toString());
      assert.isFalse(await token.whitelist(ALICE));

      await token.addLiquidity(amount, {from: ALICE, value: amount});
      assert.equal(0, await token.balanceOf(ALICE));
      assert.equal(amount, (await token.balanceOf(await token.pair())).toString());
      assert.equal(amount, (await weth.balanceOf(await token.pair())).toString());

      const pair = await Pair.at(await token.pair());
      await pair.approve(token.address, amount, {from: ALICE});
      await token.removeLiquidity(await pair.balanceOf(ALICE), {from: ALICE});
      // rounding coz empry pair
      assert.equal(amount, (toBN(await token.balanceOf(ALICE))).plus(1000).toString());
      assert.equal(1000, (await token.balanceOf(await token.pair())).toString());
      assert.equal(0, await web3.eth.getBalance(await token.pair()));
      assert.equal(0, await web3.eth.getBalance(token.address));
    });
  });
  describe("buy/sell flow", async () => {
    it("shoul buy and then sell all available sell percent, and after, get error that sell limit exceed, and after increase sell percent and sell again", async () => {
      const amountToLiquidity = ONE_BNB.multipliedBy(50);
      await token.transfer(ALICE, amountToLiquidity, {from: developerWallet});
      await token.addLiquidity(amountToLiquidity, {from: ALICE, value: amountToLiquidity});

      // buy
      const amountToBuy = ONE_BNB.multipliedBy(0.5);
      const amountToBuyAfterTax = amountToBuy.minus(amountToBuy.dividedBy(hundredPercent).multipliedBy(transferTax));
      assert.equal(0, await token.balanceOf(BOB));
      await router.swapExactETHForTokensSupportingFeeOnTransferTokens(0, [weth.address, token.address], BOB, timestamp(), {from: BOB, value:amountToBuy});

      const actualBalance = toBN(await token.balanceOf(BOB));
      // 2% rounding
      assert.isTrue(actualBalance < amountToBuyAfterTax);
      assert.isTrue(actualBalance.plus(actualBalance.dividedBy(100).multipliedBy(2)) > amountToBuyAfterTax);
      assert.equal(await token.balanceOf(BOB), ((await token.usersSellInfo(BOB)).availableSell).toString());
      assert.equal(0, (await token.usersSellInfo(BOB)).alreadySold);
      

      // sell
      const availableSellAmount = toBN(await token.availableSellAmount(BOB));
      await token.approve(router.address, availableSellAmount, {from: BOB});
      await router.swapExactTokensForETHSupportingFeeOnTransferTokens(availableSellAmount, 0, [token.address, weth.address], BOB, timestamp(), {from: BOB});      
      assert.equal(availableSellAmount, ((await token.usersSellInfo(BOB)).alreadySold).toString());
      assert.equal(0, toBN(await token.availableSellAmount(BOB)).toString());

      const errMsg = "TransferHelper: TRANSFER_FROM_FAILED";
      await truffleAssertions.reverts(router.swapExactTokensForETHSupportingFeeOnTransferTokens(availableSellAmount, 0, [token.address, weth.address], BOB, timestamp(), {from: BOB}), errMsg);

      await token.changeSellPercent(sellPercent * 2);
      assert.notEqual(0, toBN(await token.availableSellAmount(BOB)).toString());
      await token.approve(router.address, availableSellAmount, {from: BOB});
      await router.swapExactTokensForETHSupportingFeeOnTransferTokens(availableSellAmount, 0, [token.address, weth.address], BOB, timestamp(), {from: BOB});
      assert.equal(availableSellAmount.multipliedBy(2), ((await token.usersSellInfo(BOB)).alreadySold).toString());
      // rounding
      assert.equal(1, toBN(await token.availableSellAmount(BOB)).toString());
    });
  });
});