const TokenFarming = artifacts.require("StakeBnbRabbitCoin");
const RabbitCoin = artifacts.require("RabbitCoin");
const AddressFrom = artifacts.require("AddressFrom");
const ERC20Mock = artifacts.require("Token");

const Reverter = require("./helpers/reverter");
const BigNumber = require("bignumber.js");
const setCurrentTime = require("./helpers/ganacheTimeTraveler");
const truffleAssert = require("truffle-assertions");

async function advanceBlocks(amount) {
  for (let i = 0; i < amount; i++) {
    await setCurrentTime(1);
  }
}

async function getCurrentBlock() {
  return (await web3.eth.getBlock("latest")).number;
}

function toBN(num) {
  return new BigNumber(num);
}

contract("StakeBnbRabbitCoin", async (accounts) => {
  const reverter = new Reverter(web3);

  const DEFAULT = accounts[0];
  const USER1 = accounts[1];
  const USER2 = accounts[2];

  const decimal = toBN(10).pow(30);
  const oneToken = toBN(10).pow(18);

  const tokensAmount = oneToken.times(100000);

  const rewardPerBlock = oneToken.times(2);

  let stakeToken;
  let distributionToken;
  let tokenFarming;

  const name = "Rabbit Coin";
  const symbol = "RABTc";
  const transferTax = 20; // [0 - 100]
  const sellPercent = 15; // [0 - 100]
  const presaleContract = "0xC538dd8364B7D3ea01509Ee9A7f6EEf2b4C8F905";
  let stakeBnbRabbitCoinContract;
  const stakeRabbitCoinContract = "0x0000000000000000000000000000000000000002";
  const developerWallet = accounts[3];
  const marketingWallet = "0x01824A4cB1760AA579Da73434aE231946c29e74B";
  const donationWallet = "0xF0b8150123F1c58c5373f48b63E7a8eBa89b8086";
  const totalSupply = toBN("1000000000000000000000000000000");

  function getNewCumulativeSum(rewardPerBlock, totalPool, prevAP, blocksDelta) {
    return rewardPerBlock.times(decimal).idiv(totalPool).times(blocksDelta).plus(prevAP);
  }

  function getUserAggregatedReward(newAP, prevAP, userLiquidityAmount, prevReward) {
    return userLiquidityAmount.times(newAP.minus(prevAP)).idiv(decimal).plus(prevReward);
  }

  before("setup", async () => {
    addressFrom = await AddressFrom.new();
    stakeBnbRabbitCoinContract = await addressFrom.addressFrom(DEFAULT, await web3.eth.getTransactionCount(DEFAULT) + 2);

    stakeToken = await ERC20Mock.new();
    distributionToken = await RabbitCoin.new(name, symbol, transferTax, sellPercent, presaleContract, stakeBnbRabbitCoinContract, stakeRabbitCoinContract, developerWallet, marketingWallet, donationWallet, totalSupply);

    tokenFarming = await TokenFarming.new(stakeToken.address, distributionToken.address, rewardPerBlock);

    await stakeToken.transfer(USER1, tokensAmount);
    await stakeToken.transfer(USER2, tokensAmount);

    await stakeToken.approve(tokenFarming.address, tokensAmount, { from: USER1 });
    await stakeToken.approve(tokenFarming.address, tokensAmount, { from: USER2 });

    await distributionToken.transfer(tokenFarming.address, tokensAmount.times(100), {from: developerWallet});

    await reverter.snapshot();
  });

  afterEach("revert", reverter.revert);

  describe("updateRewardPerBlock", async () => {
    const stakeAmount = oneToken.times(1000);
    const blocksDelta = toBN(9);
    const newRewardPerBlock = oneToken.times(4);

    it("should correctly update reward per block", async () => {
      await tokenFarming.stake(stakeAmount, { from: USER1 });

      let totalPool = stakeAmount;

      await advanceBlocks(blocksDelta);

      await tokenFarming.stake(stakeAmount, { from: USER1 });

      let newCumulativeSum = getNewCumulativeSum(rewardPerBlock, totalPool, 0, blocksDelta.plus(1));

      assert.equal(toBN(await tokenFarming.cumulativeSum()).toString(), newCumulativeSum.toString());

      totalPool = stakeAmount.times(2);

      await advanceBlocks(blocksDelta);

      await tokenFarming.updateRewardPerBlock(newRewardPerBlock);

      newCumulativeSum = getNewCumulativeSum(rewardPerBlock, totalPool, newCumulativeSum, blocksDelta.plus(1));

      assert.equal(toBN(await tokenFarming.cumulativeSum()).toString(), newCumulativeSum.toString());
    });
  });

  describe("stake", async () => {
    const stakeAmount = oneToken.times(1000);
    const blocksDelta = toBN(9);

    it("should correctly update values", async () => {
      await tokenFarming.stake(stakeAmount, { from: USER1 });

      const userInfo = await tokenFarming.userInfos(USER1);

      assert.equal(toBN(userInfo.stakedAmount).toString(), stakeAmount.toString());
      assert.equal(toBN(userInfo.lastCumulativeSum).toString(), 0);
      assert.equal(toBN(userInfo.aggregatedReward).toString(), 0);

      assert.equal(toBN(await tokenFarming.totalPoolStaked()).toString(), stakeAmount.toString());

      assert.equal(toBN(await stakeToken.balanceOf(tokenFarming.address)).toString(), stakeAmount.toString());
      assert.equal(toBN(await stakeToken.balanceOf(USER1)).toString(), tokensAmount.minus(stakeAmount).toString());
    });

    it("should correctly update rewards", async () => {
      await tokenFarming.stake(stakeAmount, { from: USER1 });

      await advanceBlocks(blocksDelta);

      await tokenFarming.stake(stakeAmount, { from: USER1 });

      let expectedReward = rewardPerBlock.times(10);
      assert.equal(toBN((await tokenFarming.userInfos(USER1)).aggregatedReward).toString(), expectedReward.toString());

      await advanceBlocks(blocksDelta);

      await tokenFarming.stake(stakeAmount, { from: USER1 });

      expectedReward = rewardPerBlock.times(20);
      assert.equal(toBN((await tokenFarming.userInfos(USER1)).aggregatedReward).toString(), expectedReward.toString());
    });

    it("should correctly update cumulative sum and rewards", async () => {
      await tokenFarming.stake(stakeAmount, { from: USER1 });

      let totalPool = stakeAmount;

      await advanceBlocks(blocksDelta);

      await tokenFarming.stake(stakeAmount, { from: USER1 });

      let currentBlock = toBN(await getCurrentBlock());
      let newCumulativeSum = getNewCumulativeSum(rewardPerBlock, totalPool, 0, blocksDelta.plus(1));
      let aggregatedReward = getUserAggregatedReward(newCumulativeSum, 0, stakeAmount, 0);

      assert.equal(toBN(await tokenFarming.cumulativeSum()).toString(), newCumulativeSum.toString());
      assert.equal(
        toBN((await tokenFarming.userInfos(USER1)).aggregatedReward).toString(),
        aggregatedReward.toString()
      );

      totalPool = stakeAmount.times(2);

      await advanceBlocks(blocksDelta);

      await tokenFarming.stake(stakeAmount.times(2), { from: USER2 });

      currentBlock = toBN(await getCurrentBlock());
      newCumulativeSum = getNewCumulativeSum(rewardPerBlock, totalPool, newCumulativeSum, blocksDelta.plus(1));

      assert.equal(toBN(await tokenFarming.cumulativeSum()).toString(), newCumulativeSum.toString());

      totalPool = stakeAmount.times(4);

      await advanceBlocks(blocksDelta);

      const lastCumulativeSum = (await tokenFarming.userInfos(USER1)).lastCumulativeSum;

      await tokenFarming.stake(stakeAmount.times(2), { from: USER1 });

      currentBlock = toBN(await getCurrentBlock());
      newCumulativeSum = getNewCumulativeSum(rewardPerBlock, totalPool, newCumulativeSum, blocksDelta.plus(1));
      aggregatedReward = getUserAggregatedReward(
        newCumulativeSum,
        lastCumulativeSum,
        stakeAmount.times(2),
        aggregatedReward
      );

      assert.equal(toBN(await tokenFarming.cumulativeSum()).toString(), newCumulativeSum.toString());
      assert.equal(
        toBN((await tokenFarming.userInfos(USER1)).aggregatedReward).toString(),
        aggregatedReward.toString()
      );
    });
  });

  describe("withdrawFunds", async () => {
    const stakeAmount = oneToken.times(1000);
    const withdrawAmount = oneToken.times(200);
    const blocksDelta = toBN(9);

    beforeEach("setup", async () => {
      await setCurrentTime(1);
      await tokenFarming.stake(stakeAmount, { from: USER1 });
    });

    it("should correctly update values", async () => {
      await advanceBlocks(blocksDelta);
      await setCurrentTime(2419201);

      await tokenFarming.withdrawFunds(withdrawAmount, { from: USER1 });

      const userInfo = await tokenFarming.userInfos(USER1);

      const expectedBalance = stakeAmount.minus(withdrawAmount);

      assert.equal(toBN(userInfo.stakedAmount).toString(), expectedBalance.toString());

      assert.equal(toBN(await tokenFarming.totalPoolStaked()).toString(), expectedBalance.toString());

      assert.equal(toBN(await stakeToken.balanceOf(tokenFarming.address)).toString(), expectedBalance.toString());
      assert.equal(
        toBN(await stakeToken.balanceOf(USER1)).toString(),
        tokensAmount.minus(stakeAmount).plus(withdrawAmount).toString()
      );
    });

    it("should correctly update cumulative sum and rewards", async () => {
      let totalPool = stakeAmount;

      await advanceBlocks(blocksDelta - 1); // because set current time mines 1 block
      await setCurrentTime(2419201);

      await tokenFarming.withdrawFunds(withdrawAmount, { from: USER1 });

      let currentBlock = toBN(await getCurrentBlock());
      let newCumulativeSum = getNewCumulativeSum(rewardPerBlock, totalPool, 0, blocksDelta.plus(1));
      let aggregatedReward = getUserAggregatedReward(newCumulativeSum, 0, stakeAmount, 0);

      assert.equal(toBN(await tokenFarming.cumulativeSum()).toString(), newCumulativeSum.toString());
      assert.equal(
        toBN((await tokenFarming.userInfos(USER1)).aggregatedReward).toString(),
        aggregatedReward.toString()
      );

      totalPool = totalPool.minus(withdrawAmount);

      await advanceBlocks(blocksDelta);

      await tokenFarming.stake(stakeAmount.times(2), { from: USER2 });

      currentBlock = toBN(await getCurrentBlock());
      newCumulativeSum = getNewCumulativeSum(rewardPerBlock, totalPool, newCumulativeSum, blocksDelta.plus(1));

      assert.equal(toBN(await tokenFarming.cumulativeSum()).toString(), newCumulativeSum.toString());

      totalPool = stakeAmount.times(3).minus(withdrawAmount);

      await advanceBlocks(blocksDelta - 1); // because set current time mines 1 block
      await setCurrentTime(2419201);

      const lastCumulativeSum = (await tokenFarming.userInfos(USER1)).lastCumulativeSum;

      await tokenFarming.withdrawFunds(withdrawAmount, { from: USER1 });

      currentBlock = toBN(await getCurrentBlock());
      newCumulativeSum = getNewCumulativeSum(rewardPerBlock, totalPool, newCumulativeSum, blocksDelta.plus(1));
      aggregatedReward = getUserAggregatedReward(
        newCumulativeSum,
        lastCumulativeSum,
        stakeAmount.minus(withdrawAmount),
        aggregatedReward
      );

      assert.equal(toBN(await tokenFarming.cumulativeSum()).toString(), newCumulativeSum.toString());
      assert.equal(
        toBN((await tokenFarming.userInfos(USER1)).aggregatedReward).toString(),
        aggregatedReward.toString()
      );
    });

    it("should get exception if the user does not have enough tokens", async () => {
      await setCurrentTime(2419201);
      const reason = "Farming: Not enough staked tokens to withdraw";
      await truffleAssert.reverts(tokenFarming.withdrawFunds(withdrawAmount.times(6), { from: USER1 }), reason);
    });
  });

  describe("claimRewards", async () => {
    const stakeAmount = oneToken.times(1000);
    const blocksDelta = toBN(18);

    beforeEach("setup", async () => {
      await tokenFarming.stake(stakeAmount, { from: USER1 });

      await advanceBlocks(blocksDelta);

      await tokenFarming.stake(stakeAmount, { from: USER1 });
    });

    it("should correctly claim rewards", async () => {
      await tokenFarming.claimRewards({ from: USER1 });

      const expectedReward = oneToken.times(40);
      assert.equal(toBN(await distributionToken.balanceOf(USER1)).toString(), expectedReward.toString());
      assert.equal(toBN((await tokenFarming.userInfos(USER1)).aggregatedReward).toString(), 0);
    });

    it("should claim rewards several times", async () => {
      await tokenFarming.claimRewards({ from: USER1 });

      await advanceBlocks(blocksDelta.plus(1));

      await tokenFarming.claimRewards({ from: USER1 });

      const expectedReward = oneToken.times(80);
      assert.equal(toBN(await distributionToken.balanceOf(USER1)).toString(), expectedReward.toString());
      assert.equal(toBN((await tokenFarming.userInfos(USER1)).aggregatedReward).toString(), 0);
    });

    it("shoul get exception if the user has no rewards", async () => {
      const reason = "Farming: Nothing to claim";
      await truffleAssert.reverts(tokenFarming.claimRewards({ from: USER2 }), reason);
    });
  });

  describe("getLatestUserRewards", async () => {
    const stakeAmount = oneToken.times(1000);
    const blocksDelta = toBN(18);

    it("should corectly return current user rewards", async () => {
      await tokenFarming.stake(stakeAmount, { from: USER1 });

      await advanceBlocks(blocksDelta);

      await tokenFarming.stake(stakeAmount, { from: USER1 });

      const rewards = await tokenFarming.getLatestUserRewards(USER1);

      assert.equal(toBN((await tokenFarming.userInfos(USER1)).aggregatedReward).toString(), rewards.toString());
    });
  });
});