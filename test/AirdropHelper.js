const AirdropHelper = artifacts.require("AirdropHelper");
const Token = artifacts.require("Token");

const Reverter = require("./helpers/reverter");
const truffleAssertions = require("truffle-assertions");

contract("AirdropHelper", async (accounts) => {
  const reverter = new Reverter(web3);

  let airdropHelper;
  let token;

  before("setup", async () => {
    airdropHelper = await AirdropHelper.new();
    token = await Token.new();
    await reverter.snapshot();
  });

  afterEach("revert", reverter.revert);

  describe("multisendToken", async () => {
    it("shoult transfer between accounts", async () => {
      const receivers = [accounts[1], accounts[2], accounts[3], accounts[4], accounts[5]]
      const amounts = [10, 20, 30, 40, 50]

      for(let i = 0; i < receivers.length; ++i) {
        assert.equal(0, await token.balanceOf(receivers[i]))
      }

      await token.approve(airdropHelper.address, "1111111111111")
      await airdropHelper.multisendToken(token.address, receivers, amounts);

      for(let i = 0; i < receivers.length; ++i) {
        assert.equal(amounts[i], await token.balanceOf(receivers[i]))
      }
    });
    it("shoult get exception, length not equal", async () => {
      const revertMsg = "length not equal";
      const receivers = [accounts[1], accounts[2], accounts[3], accounts[4], accounts[5]]
      const amounts = [10, 20, 30, 40]

      await truffleAssertions.reverts(airdropHelper.multisendToken(token.address, receivers, amounts), revertMsg)
    });
  });
});
